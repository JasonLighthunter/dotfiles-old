#!/bin/bash

export PKG=(
  feh
  redshift
  i3-gaps
  picom
  i3status
  kitty
  neofetch
  adobe-source-han-sans-otc-fonts
  syncthing
  ttf-fira-code
  ttf-font-awesome
  ttf-joypixels
  xorg-xinit
  fish
)
