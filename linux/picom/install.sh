echo "make sure ~/.config/picom exists"
mkdir -p ~/.config/picom

echo "removing old picom config file"
rm -rf ~/.config/picom/picom.conf

echo "linking ~/dotfiles/linux/picom/picom.conf to ~/.config/picom/picom.conf"
ln -sfT ~/dotfiles/linux/picom/picom.conf ~/.config/picom/picom.conf

