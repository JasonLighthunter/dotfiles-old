
echo "### redshift ###"
echo "make sure ~/.config/redshift exists"
mkdir -p ~/.config/redshift

echo "removing old redshift config file"
rm -rf ~/.config/redshift/redshift.conf

echo "linking ~/dotfiles/linux/redshift/redshift.conf"
ln -sfT ~/dotfiles/linux/redshift/redshift.conf ~/.config/redshift/redshift.conf
