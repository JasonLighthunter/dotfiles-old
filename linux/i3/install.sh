
echo "### i3 ###"
echo "make sure ~/.config/i3 exists"
mkdir -p ~/.config/i3

echo "removing old i3 config file"
rm -rf ~/.config/i3/config

echo "linking ~/dotfiles/linux/i3/config"
ln -sfT ~/dotfiles/linux/i3/config ~/.config/i3/config


echo "### i3-status ###"

echo "make sure ~/.config/i3status exists"
mkdir -p ~/.config/i3status

echo "removing old i3 config file"
rm -rf ~/.config/i3i3status/config

echo "linking ~/dotfiles/linux/i3/status_config"
ln -sfT ~/dotfiles/linux/i3/status_config ~/.config/i3status/config

