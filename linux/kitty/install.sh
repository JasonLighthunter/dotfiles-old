echo "make sure ~/.config/kitty exists"
mkdir -p ~/.config/kitty

echo "removing old kitty config file"
rm -rf ~/.config/kitty/kitty.conf

echo "linking ~/dotfiles/linux/kitty/kitty.conf ~/.config/kitty/kitty.conf"
ln -sfT ~/dotfiles/linux/kitty/kitty.conf ~/.config/kitty/kitty.conf

