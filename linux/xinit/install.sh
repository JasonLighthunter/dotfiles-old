echo "removing old .xinitrc config file"
rm -rf ~/.xinitrc

echo "linking ~/dotfiles/linux/xinit/.xinitrc to ~/.xinitrc"
ln -sfT ~/dotfiles/linux/xinit/.xinitrc ~/.xinitrc

echo "renaming /etc/profile to /etc/profile.old"
sudo mv /etc/profile /etc/profile.old

echo "linking ~/dotfiles/linux/profile/ to /etc/profile"
sudo ln -sfT ~/dotfiles/linux/profile /etc/profile
