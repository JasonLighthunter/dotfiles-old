#!/usr/bin/sh

. ~/dotfiles/linux/packages.sh

echo "Updating packages..."
sudo pacman -Syu --needed --noconfirm

for pkg in "${PKG[@]}"
do
  echo "Installing ${pkg}..."
  sudo pacman -Syu "$pkg" --needed --noconfirm
done

echo "generating and setting locale"
sudo localectl set-locale LANG=en_US.UTF8

. ~/dotfiles/linux/i3/install.sh
. ~/dotfiles/linux/xinit/install.sh
. ~/dotfiles/linux/kitty/install.sh
. ~/dotfiles/linux/picom/install.sh
. ~/dotfiles/linux/neofetch/install.sh
. ~/dotfiles/linux/redshift/install.sh

echo "TODO List"
echo "- Check if everything is working"
echo "- Enable Redshift?"
echo "- Install Graphics Drivers?"
echo "- Automate install of fish"
