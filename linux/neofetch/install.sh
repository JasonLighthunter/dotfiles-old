
echo "### neofetch ###"
echo "make sure ~/.config/neofetch exists"
mkdir -p ~/.config/neofetch

echo "removing old neofetch config file"
rm -rf ~/.config/neofetch/config.conf

echo "linking ~/dotfiles/linux/neofetch/config.conf"
ln -sfT ~/dotfiles/linux/neofetch/config.conf ~/.config/neofetch/config
