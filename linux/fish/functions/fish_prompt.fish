function fish_prompt
	set_color FF0000
	echo -n (pwd)
	set_color normal
	echo (fish_git_prompt)
	set_color BBBBBB
	echo "-> "
end
